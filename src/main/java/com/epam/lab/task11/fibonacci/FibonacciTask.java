package com.epam.lab.task11.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class FibonacciTask {
    private static final int FIBONACCI_SET_NUMBER = 9;
    private static final int NUMBER_OF_THREADS = 5;
    static volatile int number;
    private static final Logger LOGGER = LogManager.getLogger(FibonacciTask.class);

    public FibonacciTask(int n) {
        number = n;
    }

    public void printFibonacciSequenceInFewThreads() {
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            new FibonacciThread().start();
        }
    }

    public void printFibonacciSequenceWithSingleThreadExecutor() {
        LOGGER.info("SingleThreadScheduledExecutor:");
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            Executors.newSingleThreadScheduledExecutor().execute(new FibonacciThread());
        }
    }

    public void printFibonacciSequenceWithFixedThreadPool() {
        LOGGER.info("FixedThreadPool:");
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            Executors.newFixedThreadPool(3).execute(new FibonacciThread());
        }
    }

    public void printFibonacciSequenceWithCachedThreadPool() {
        LOGGER.info("CachedThreadPool:");
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            Executors.newCachedThreadPool().execute(new FibonacciThread());
        }
    }

    public void printFibonacciSequenceWithScheduledThreadPool() {
        LOGGER.info("ScheduledThreadPool:");
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            Executors.newScheduledThreadPool(3).schedule(new FibonacciThread(), 1, TimeUnit.SECONDS);
        }
    }

    public void printSumFibonacciSequence() {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        for (int i = 0; i < NUMBER_OF_THREADS; i++) {
            try {
                LOGGER.info(executor.submit(new FibonacciCallable()).get());
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        FibonacciTask fibonacciTask = new FibonacciTask(FIBONACCI_SET_NUMBER);
        fibonacciTask.printFibonacciSequenceInFewThreads();
        fibonacciTask.printFibonacciSequenceWithCachedThreadPool();
        fibonacciTask.printFibonacciSequenceWithFixedThreadPool();
        fibonacciTask.printFibonacciSequenceWithScheduledThreadPool();
        fibonacciTask.printFibonacciSequenceWithSingleThreadExecutor();
        fibonacciTask.printSumFibonacciSequence();
    }
}
