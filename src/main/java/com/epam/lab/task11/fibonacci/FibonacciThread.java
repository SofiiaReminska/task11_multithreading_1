package com.epam.lab.task11.fibonacci;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FibonacciThread extends Thread {
    private static final Logger LOGGER = LogManager.getLogger(FibonacciThread.class);

    @Override
    public void run() {
        StringBuilder sb = new StringBuilder();
        int fib1 = 0;
        int fib2 = 1;
        int fib3 = 1;
        for (int i = 0; i < FibonacciTask.number; i++) {
            sb.append(fib3).append(" ");
            fib3 = fib1 + fib2;
            fib1 = fib2;
            fib2 = fib3;
        }
        LOGGER.info(sb.toString());
    }
}
