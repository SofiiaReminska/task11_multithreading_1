package com.epam.lab.task11.fibonacci;

import java.util.concurrent.Callable;

public class FibonacciCallable implements Callable<Long> {
    @Override
    public Long call() throws Exception {
        long first = 0;
        long second = 1;
        long fibonacci;
        long sum = 0;
        for (int i = 0; i < FibonacciTask.number; i++) {
            fibonacci = first + second;
            first = second;
            second = fibonacci;
            sum += fibonacci;
        }
        return sum;
    }
}
