package com.epam.lab.task11.pingpong;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PingPong {
    private static final Logger LOGGER = LogManager.getLogger(PingPong.class);
    private static final int MAX = 10;
    private volatile int counter = 0;
    private boolean ponged = true;

    private synchronized void ping() {
        try {
            if (ponged) {
                counter++;
                LOGGER.info("Ping");
                ponged = false;
            }
            wait();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private synchronized void pong() {
        if (!ponged) {
            counter++;
            LOGGER.info("Pong");
            ponged = true;
        }
        notify();
    }

    public static void main(String... arg) {
        final PingPong pingpong = new PingPong();

        Thread ping = new Thread(() -> {
            while (pingpong.counter < MAX)
                pingpong.ping();
        });
        Thread pong = new Thread(() -> {
            while (pingpong.counter < MAX)
                pingpong.pong();
        });
        ping.start();
        pong.start();
    }
}
