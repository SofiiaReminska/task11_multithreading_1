package com.epam.lab.task11.synchronize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeMultipleObjects {
    private static final Logger LOGGER = LogManager.getLogger(SynchronizeMultipleObjects.class);
    private static volatile String usualString, upperCaseString, lowerCaseString;

    public SynchronizeMultipleObjects(String first, String second) {
        usualString = String.format("%s %s", first, second);
        lowerCaseString = String.format("%s %s", first, second);
        upperCaseString = String.format("%s %s", first, second);
    }

    public void doSomeWork() {
        Thread toUpperCaseThread = new Thread(this::toUpperCaseMethod);
        Thread toLowerCaseThread = new Thread(this::toLowerCaseMethod);
        Thread usualThread = new Thread(this::usualMethod);
        toUpperCaseThread.start();
        toLowerCaseThread.start();
        usualThread.start();
    }

    private void toUpperCaseMethod() {
        String tmp;
        synchronized (upperCaseString) {
            for (int i = 0; i < 10; i++) {
                tmp = upperCaseString.toUpperCase();
                LOGGER.info(tmp);
            }
        }
    }

    private void toLowerCaseMethod() {
        String tmp;
        synchronized (lowerCaseString) {
            for (int i = 0; i < 10; i++) {
                tmp = lowerCaseString.toLowerCase();
                LOGGER.info(tmp);
            }
        }
    }

    private void usualMethod() {
        String tmp;
        synchronized (usualString) {
            for (int i = 0; i < 10; i++) {
                tmp = usualString;
                LOGGER.info(tmp);
            }
        }
    }

    public static void main(String[] args) {
        SynchronizeMultipleObjects sync = new SynchronizeMultipleObjects("Sofiia", "Reminska");
        sync.doSomeWork();
    }
}
