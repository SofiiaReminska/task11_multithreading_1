package com.epam.lab.task11.synchronize;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SynchronizeOneObjectTask {
    private static final Logger LOGGER = LogManager.getLogger(SynchronizeOneObjectTask.class);
    private static volatile String str;

    public SynchronizeOneObjectTask(String first, String second) {
        str = String.format("%s %s", first, second);
    }

    public void doSomeWork() {
        Thread toUpperCaseThread = new Thread(this::toUpperCaseMethod);
        Thread toLowerCaseThread = new Thread(this::toLowerCaseMethod);
        Thread usualThread = new Thread(this::usualMethod);
        toUpperCaseThread.start();
        toLowerCaseThread.start();
        usualThread.start();
    }

    private void toUpperCaseMethod() {
        String tmp;
        synchronized (str) {
            for (int i = 0; i < 10; i++) {
                tmp = str.toUpperCase();
                LOGGER.info(tmp);
            }
        }
    }

    private void toLowerCaseMethod() {
        String tmp;
        synchronized (str) {
            for (int i = 0; i < 10; i++) {
                tmp = str.toLowerCase();
                LOGGER.info(tmp);
            }
        }
    }

    private void usualMethod() {
        String tmp;
        synchronized (str) {
            for (int i = 0; i < 10; i++) {
                tmp = str;
                LOGGER.info(tmp);
            }
        }
    }

    public static void main(String[] args) {
        SynchronizeOneObjectTask sync = new SynchronizeOneObjectTask("Sofiia", "Reminska");
        sync.doSomeWork();
    }
}
