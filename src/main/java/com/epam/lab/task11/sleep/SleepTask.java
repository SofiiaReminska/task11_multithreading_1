package com.epam.lab.task11.sleep;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class SleepTask {
    private static final Logger LOGGER = LogManager.getLogger(SleepTask.class);
    private static final int POOL_SIZE = 5;

    private static void sleepThreadRandomTime(int poolSize) {

        for (int i = 0; i < poolSize; i++) {
            int randomTime = (int) (Math.random() * 9 + 1);
            Thread sleepingThread = new Thread(() ->
            {
                try {
                    Thread.sleep(randomTime * 1000);
                    LOGGER.info("Thread {} sleep for {} seconds", Thread.currentThread().getId(), randomTime);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            });
            Executors.newScheduledThreadPool(poolSize).schedule(sleepingThread, randomTime, TimeUnit.SECONDS);
        }
    }

    public static void main(String[] args) {
        sleepThreadRandomTime(POOL_SIZE);
    }
}
